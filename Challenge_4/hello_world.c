/**
 * The linux/module.h is needed for the printk macro
 * The linux/kernel.h is needed for the KERNEL_INFO macro
 *
 * The init_module and cleanup_module function points the entry and exit points 
**/
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>


int module_begin(void)
{
        printk(KERN_INFO "Hello Kernel\n");
        return 0;
}


void module_end(void)
{
        printk(KERN_INFO "Bye Kernel\n");
}


module_init(module_begin);
module_exit(module_end);


MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("Solution to Problem !");
MODULE_AUTHOR("7c1caf2f50d1");
